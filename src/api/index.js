import axios from 'axios'

axios.interceptors.request.use(function(config) {
    const token = sessionStorage.getItem('token')
    if (token) {
        config.headers.Authorization = token
        config.headers['Content-Type'] = 'application/json'
    }
    return config
})

export function fetch_login(paramas) {
  return axios.post('http://23.225.140.154:3001/login', paramas)
}
export function login_manage(paramas) {
  return axios.post('http://23.225.140.154:3001/loginmanage', paramas)
}
