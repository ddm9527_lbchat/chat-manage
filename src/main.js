// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'

import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
Vue.use(ElementUI);


import './assets/base.css'

router.beforeEach((to, from, next) => {
  // 在此处就需要判断, 是否能进入一些禁地(需要登录的页面)
  // console.log(to, from)
  // 如果添加了导航守卫的回调函数
  // 必须调用next函数  将其引导到某个页面, 如果不传参数就是不干预路由跳转
  let token = localStorage.getItem('token')
  if (!token && to.path !== '/login') {
    // console.log('我在疯狂的进login')
    return next('/login')
  }
  next()
})


Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
